module Test.Merkle exposing (tests)

import Expect exposing (..)
import Test exposing (..)
import Json.Encode exposing (..)
import Json.Decode exposing (..)
import List exposing (map)
import Merkle exposing (..)
import SHA exposing (sha256sum)


tests : Test
tests =
    describe "Merkle Trees" <|
        List.concat
            [ dataTreeInt
                |> List.map
                    (\( msg, t1, t2 ) ->
                        test msg <| \() -> t2 |> Expect.equal t1
                    )
            , dataBool
                |> List.map
                    (\( msg, b1, b2 ) ->
                        test msg <| \() -> b2 |> Expect.equal b1
                    )
            , dataListIntString
                |> List.map
                    (\( msg, lis1, lis2 ) ->
                        test msg <| \() -> lis2 |> Expect.equal lis1
                    )
            , dataInt
                |> List.map
                    (\( msg, lis1, lis2 ) ->
                        test msg <| \() -> lis2 |> Expect.equal lis1
                    )
            , dataToJson
                |> List.map
                    (\( msg, lis1, lis2 ) ->
                        test msg <| \() -> lis2 |> Expect.equal lis1
                    )
            , dataFromJson
                |> List.map
                    (\( msg, lis1, lis2 ) ->
                        test msg <| \() -> lis2 |> Expect.equal lis1
                    )
            ]


dataTreeInt : List ( String, Tree Int, Tree Int )
dataTreeInt =
    [ ( "initialize = initialize (sha256sum)"
      , initialize Nothing Json.Encode.int
      , initialize (Just ([ sha256sum ])) Json.Encode.int
      )
    , ( "singleton = initialize + 1 insert"
      , singleton 42 Nothing Json.Encode.int
      , initialize Nothing Json.Encode.int
            |> insert 42
      )
    , ( "singleton = initialize + 1 insert"
      , singleton 42 (Just ([ sha256sum ])) Json.Encode.int
      , initialize (Just ([ sha256sum ])) Json.Encode.int
            |> insert 42
      )
    , ( "singleton = initialize + 1 insert"
      , singleton 42 (Just ([ identity ])) Json.Encode.int
      , initialize (Just ([ identity ])) Json.Encode.int
            |> insert 42
      )
    , ( "fromList = initialize + 3 inserts"
      , List.range 40 42 |> fromList Nothing Json.Encode.int
      , initialize Nothing Json.Encode.int
            |> insert 40
            |> insert 41
            |> insert 42
      )
    , ( "fromList = initialize + 3 inserts"
      , List.range 40 42 |> fromList (Just ([ sha256sum ])) Json.Encode.int
      , initialize (Just ([ sha256sum ])) Json.Encode.int
            |> insert 40
            |> insert 41
            |> insert 42
      )
    , ( "fromList = initialize + 3 inserts"
      , List.range 40 42 |> fromList (Just ([ identity ])) Json.Encode.int
      , initialize (Just ([ identity ])) Json.Encode.int
            |> insert 40
            |> insert 41
            |> insert 42
      )
    , ( "insertFromList = initialize + 3 inserts"
      , List.range 40 42
            |> insertFromList
                (initialize Nothing Json.Encode.int)
      , initialize Nothing Json.Encode.int
            |> insert 40
            |> insert 41
            |> insert 42
      )
    ]


dataBool : List ( String, Bool, Bool )
dataBool =
    [ ( "initialize contains 42 = False"
      , False
      , initialize Nothing Json.Encode.int
            |> contains 42
      )
    , ( "singleton contains 42 = True"
      , True
      , singleton 42 Nothing Json.Encode.int
            |> contains 42
      )
    ]


dataListIntString : List ( String, List ( Int, String ), List ( Int, String ) )
dataListIntString =
    [ ( "initialize get 42 = []"
      , []
      , initialize Nothing Json.Encode.int
            |> get 42
      )
    , ( "singleton get 42 = [(42,\"73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049\")]"
      , [ ( 42, "73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049" ) ]
      , singleton 42 Nothing Json.Encode.int
            |> get 42
      )
    , ( "initialize flatten = []"
      , []
      , initialize Nothing Json.Encode.int
            |> flatten
      )
    , ( "singleton flatten = [(42,\"73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049\")]"
      , [ ( 42, "73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049" ) ]
      , singleton 42 Nothing Json.Encode.int
            |> flatten
      )
    ]


dataInt : List ( String, Int, Int )
dataInt =
    [ ( "initialize depth = 0"
      , 0
      , initialize Nothing Json.Encode.int
            |> depth
      )
    , ( "singleton depth = 0 (2^0 = 1 at most)"
      , 0
      , singleton 42 Nothing Json.Encode.int
            |> depth
      )
    , ( "initialize + 8 elements, depth = 3 (2^3 = 8)"
      , 3
      , List.range 40 47
            |> insertFromList
                (initialize Nothing Json.Encode.int)
            |> depth
      )
    , ( "initialize + 9 elements, depth = 4 (2^4 = 16)"
      , 4
      , List.range 40 48
            |> insertFromList
                (initialize Nothing Json.Encode.int)
            |> depth
      )
    ]


validTree =
    """{"count":3,"hash":"37536cfc05b5fe76ab5bddc19bca3f9714a6dbe3aa65fd94f876f79b55b6ff5e","left":{"count":2,"hash":"ae412269dac52f776954738908631fbf1f4f849a7492598bc3193b7ab6505017","left":{"hash":"d59eced1ded07f84c145592f65bdf854358e009c5cd705f5215bf18697fed103","data":40},"right":{"hash":"3d914f9348c9cc0ff8a79716700b9fcd4d2f3e711608004eb8f138bcba7f14d9","data":41}},"right":{"count":1,"hash":"c6e4fe6740bd8b19e6c2edb0151d9a823089403497df939b8ca619c23c037d5a","left":{"hash":"73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049","data":42},"right":null}}"""


invalidTree =
    """{"count":3,"hash":"THIS IS INVALID !!! FAIL FISH !!!","left":{"count":2,"hash":"ae412269dac52f776954738908631fbf1f4f849a7492598bc3193b7ab6505017","left":{"hash":"d59eced1ded07f84c145592f65bdf854358e009c5cd705f5215bf18697fed103","data":40},"right":{"hash":"3d914f9348c9cc0ff8a79716700b9fcd4d2f3e711608004eb8f138bcba7f14d9","data":41}},"right":{"count":1,"hash":"c6e4fe6740bd8b19e6c2edb0151d9a823089403497df939b8ca619c23c037d5a","left":{"hash":"73475cb40a568e8da8a045ced110137e159f890ac4da883b6b17dc651b3a8049","data":42},"right":null}}"""


dataToJson : List ( String, String, String )
dataToJson =
    [ ( "initialize + 3 elements = valid JSON tree"
      , validTree
      , initialize Nothing Json.Encode.int
            |> insert 40
            |> insert 41
            |> insert 42
            |> toJson 0 0
      )
    ]


dataFromJson : List ( String, Bool, Bool )
dataFromJson =
    [ ( "invalid JSON tree is parsed Ok but is not consistent"
      , False
      , case (invalidTree |> fromJson Nothing Json.Encode.int Json.Decode.int) of
            Ok t ->
                t |> isValid Nothing

            Err msg ->
                Debug.crash msg
      )
    ]
