port module Main exposing (..)

import Json.Encode exposing (Value)
import Test exposing (..)
import Test.Runner.Node exposing (TestProgram, run)
import Test.Merkle as Merkle


tests : Test
tests =
    describe "Tests for Merkle Tree library"
        [ Merkle.tests
        ]


main : TestProgram
main =
    run emit tests


port emit : ( String, Value ) -> Cmd msg
