# 2.0.1

### Upgraded to 0.18

# 2.0.0

### Code, Documentation and Test

  * Added example based on JSON Values

  * Changed as the decoder is not neccesary:

	- fromList : Maybe.Maybe (List (String -> String)) -> (a -> Json.Encode.Value) -> Json.Decode.Decoder a -> List a -> Merkle.Tree a

	+ fromList : Maybe.Maybe (List (String -> String)) -> (a -> Json.Encode.Value) -> List a -> Merkle.Tree a

	- initialize : Maybe.Maybe (List (String -> String)) -> (a -> Json.Encode.Value) -> Json.Decode.Decoder a -> Merkle.Tree a

	+ initialize : Maybe.Maybe (List (String -> String)) -> (a -> Json.Encode.Value) -> Merkle.Tree a

	- singleton : a -> Maybe.Maybe (List (String -> String)) -> (a -> Json.Encode.Value) -> Json.Decode.Decoder a -> Merkle.Tree a

	+ singleton : a -> Maybe.Maybe (List (String -> String)) -> (a -> Json.Encode.Value) -> Merkle.Tree a

# 1.0.3

### Test

  * Added a few test cases just to ensure usability

# 1.0.2

### Documentation

  * Removed unnecesary text from code file

  * Removed quote block from `Note`s

  * Fixed `JSON` to be code block instead of text

# 1.0.1

### Documentation

  * Named correctly in README.md

# 1.0.0

### Initial release

  * Added the following type to `Merkle`:

	Tree :
	{ List (String -> String)
 	, a -> Json.Encode.Value
	, Json.Decode.Decoder a
	, BinaryTree a }

  * Added the following functions to `Merkle`:

	initialize :
	    Maybe (List (String -> String))
	    -> (a -> Value)
	    -> Decoder a
	    -> Tree a

        singleton :
	    a
	    -> Maybe (List (String -> String))
	    -> (a -> Value)
	    -> Decoder a
	    -> Tree a

        fromList :
	    Maybe (List (String -> String))
	    -> (a -> Value)
	    -> Decoder a
	    -> List a
	    -> Tree a

        insert : a -> Tree a -> Tree a

        insertFromList : Tree a -> List a -> Tree a

        contains : a -> Tree a -> Bool

        get : a -> Tree a -> List ( a, String )

        flatten : Tree a -> List ( a, String )

        depth : Tree a -> Int

        isValid : Maybe (List (String -> String)) -> Tree a -> Bool

        toJson : Int -> Int -> Tree a -> String

        fromJson :
	    Maybe (List (String -> String))
	    -> (a -> Value)
	    -> Decoder a
	    -> String
	    -> Result String (Tree a)
