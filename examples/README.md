# Run the Examples

To run the examples in this folder, follow the following steps:

```bash
git clone https://github.com/spisemisu/elm-merkletree.git (or git@github.com:spisemisu/elm-merkletree.git)
cd elm-merkletree
cd examples
elm-reactor
```

This will navigate into the `examples/` directory and start `elm-reactor`. From
here, go to [http://localhost:8000](http://localhost:8000) and start clicking on
`.elm` files to see them in action.

# Merkle Tree of Json Values

We have implemented the example from the *Technology at
GDS* blog post: 
[Guaranteeing the integrity of a register](https://gdstechnology.blog.gov.uk/2015/10/13/guaranteeing-the-integrity-of-a-register/).

We start out by defining the reviews, represented as `Json.Encode.Value`'s:

```elm
( a, b, c, d, e ) =
    ( Json.Encode.object
        [ ( "name", Json.Encode.string "Bridge Cafe" )
        , ( "rating", Json.Encode.float 4.0 )
        , ( "date", "2014-02-20T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "Prima Doner" )
        , ( "rating", Json.Encode.float 2.0 )
        , ( "date", "2014-04-15T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "The Bull" )
        , ( "rating", Json.Encode.float 3.0 )
        , ( "date", "2014-06-05T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "The Tall Ship" )
        , ( "rating", Json.Encode.float 5.0 )
        , ( "date", "2014-10-30T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "Roy's Rolls" )
        , ( "rating", Json.Encode.float 3.0 )
        , ( "date", "2015-01-10T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    )
f : Json.Encode.Value
f =
    Json.Encode.object
        [ ( "name", Json.Encode.string "Prima Doner" )
        , ( "rating", Json.Encode.float 4.0 )
        , ( "date", "2015-02-12T00:00:00.0000000Z" |> Json.Encode.string )
        ]
```

We then instantiate our `Merkle.tree` from a list of the defined values (we
don't add `f` yet).

```elm
tree : Tree Json.Encode.Value
tree =
    [ a, b, c, d, e ]
        |> Merkle.fromList Nothing identity
```

The output we get, `JSON`, looks like:

```text
Merkle Tree of Json Values (before f inserted): {
    "count": 5,
    "hash": "6d3a9b0",
    "left": {
        "count": 4,
        "hash": "4bacec6",
        "left": {
            "count": 2,
            "hash": "111a9fe",
            "left": {
                "hash": "5130e55",
                "data": {
                    "name": "Bridge Cafe",
                    "rating": 4,
                    "date": "2014-02-20T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "9fdce19",
                "data": {
                    "name": "Prima Doner",
                    "rating": 2,
                    "date": "2014-04-15T00:00:00.0000000Z"
                }
            }
        },
        "right": {
            "count": 2,
            "hash": "56a8994",
            "left": {
                "hash": "3238271",
                "data": {
                    "name": "The Bull",
                    "rating": 3,
                    "date": "2014-06-05T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "b54b483",
                "data": {
                    "name": "The Tall Ship",
                    "rating": 5,
                    "date": "2014-10-30T00:00:00.0000000Z"
                }
            }
        }
    },
    "right": {
        "count": 1,
        "hash": "de969fe",
        "left": {
            "count": 1,
            "hash": "a43077c",
            "left": {
                "hash": "916ee1e",
                "data": {
                    "name": "Roy's Rolls",
                    "rating": 3,
                    "date": "2015-01-10T00:00:00.0000000Z"
                }
            },
            "right": null
        },
        "right": null
    }
}
```

Which matches the drawing made by the *GDS*:

![](../assets/imgs/ct-log1-merkle.png?raw=true)

We can now insert `f` into the tree:

```elm
tree' : Tree Json.Encode.Value
tree' =
    tree |> Merkle.insert f
```

getting the following `JSON` output:

```
Merkle Tree of Json Values (after f inserted): {
    "count": 6,
    "hash": "14f8fd6",
    "left": {
        "count": 4,
        "hash": "4bacec6",
        "left": {
            "count": 2,
            "hash": "111a9fe",
            "left": {
                "hash": "5130e55",
                "data": {
                    "name": "Bridge Cafe",
                    "rating": 4,
                    "date": "2014-02-20T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "9fdce19",
                "data": {
                    "name": "Prima Doner",
                    "rating": 2,
                    "date": "2014-04-15T00:00:00.0000000Z"
                }
            }
        },
        "right": {
            "count": 2,
            "hash": "56a8994",
            "left": {
                "hash": "3238271",
                "data": {
                    "name": "The Bull",
                    "rating": 3,
                    "date": "2014-06-05T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "b54b483",
                "data": {
                    "name": "The Tall Ship",
                    "rating": 5,
                    "date": "2014-10-30T00:00:00.0000000Z"
                }
            }
        }
    },
    "right": {
        "count": 2,
        "hash": "1eee38b",
        "left": {
            "count": 2,
            "hash": "fdd4753",
            "left": {
                "hash": "916ee1e",
                "data": {
                    "name": "Roy's Rolls",
                    "rating": 3,
                    "date": "2015-01-10T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "7ff4830",
                "data": {
                    "name": "Prima Doner",
                    "rating": 4,
                    "date": "2015-02-12T00:00:00.0000000Z"
                }
            }
        },
        "right": null
    }
}
```

Which also matches the drawing made by the *GDS*:

![](../assets/imgs/ct-merkle6.png?raw=true)

To show how simple it is to *export/import* these `Merkle.Tree`'s, we show how
both trees are converted `toJson` and `fromJson` by using the `Elm` built-in
`Json.Decode.value` as the `JSON` decoder. It's also worth mentioning that we
also didn't have to built any fancy `JSON` encoder, as we also just use `Elm`
built in `identity` function. That is pretty cool right?

```elm
json1 : Tree Json.Encode.Value
json1 =
    case
        (tree
            |> Merkle.toJson 0 0
            |> Merkle.fromJson Nothing identity Json.Decode.value
        )
    of
        Ok t ->
            t

        Err msg ->
            Debug.crash msg
```

```
Merkle Tree of Json Values (to and from JSON): {
    "count": 5,
    "hash": "6d3a9b0",
    "left": {
        "count": 4,
        "hash": "4bacec6",
        "left": {
            "count": 2,
            "hash": "111a9fe",
            "left": {
                "hash": "5130e55",
                "data": {
                    "name": "Bridge Cafe",
                    "rating": 4,
                    "date": "2014-02-20T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "9fdce19",
                "data": {
                    "name": "Prima Doner",
                    "rating": 2,
                    "date": "2014-04-15T00:00:00.0000000Z"
                }
            }
        },
        "right": {
            "count": 2,
            "hash": "56a8994",
            "left": {
                "hash": "3238271",
                "data": {
                    "name": "The Bull",
                    "rating": 3,
                    "date": "2014-06-05T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "b54b483",
                "data": {
                    "name": "The Tall Ship",
                    "rating": 5,
                    "date": "2014-10-30T00:00:00.0000000Z"
                }
            }
        }
    },
    "right": {
        "count": 1,
        "hash": "de969fe",
        "left": {
            "count": 1,
            "hash": "a43077c",
            "left": {
                "hash": "916ee1e",
                "data": {
                    "name": "Roy's Rolls",
                    "rating": 3,
                    "date": "2015-01-10T00:00:00.0000000Z"
                }
            },
            "right": null
        },
        "right": null
    }
}
```

```elm
json2 : Tree Json.Encode.Value
json2 =
    case
        (tree'
            |> Merkle.toJson 0 0
            |> Merkle.fromJson Nothing identity Json.Decode.value
        )
    of
        Ok t ->
            t

        Err msg ->
            Debug.crash msg
```

```
Merkle Tree of Json Values (to and from JSON): {
    "count": 6,
    "hash": "14f8fd6",
    "left": {
        "count": 4,
        "hash": "4bacec6",
        "left": {
            "count": 2,
            "hash": "111a9fe",
            "left": {
                "hash": "5130e55",
                "data": {
                    "name": "Bridge Cafe",
                    "rating": 4,
                    "date": "2014-02-20T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "9fdce19",
                "data": {
                    "name": "Prima Doner",
                    "rating": 2,
                    "date": "2014-04-15T00:00:00.0000000Z"
                }
            }
        },
        "right": {
            "count": 2,
            "hash": "56a8994",
            "left": {
                "hash": "3238271",
                "data": {
                    "name": "The Bull",
                    "rating": 3,
                    "date": "2014-06-05T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "b54b483",
                "data": {
                    "name": "The Tall Ship",
                    "rating": 5,
                    "date": "2014-10-30T00:00:00.0000000Z"
                }
            }
        }
    },
    "right": {
        "count": 2,
        "hash": "1eee38b",
        "left": {
            "count": 2,
            "hash": "fdd4753",
            "left": {
                "hash": "916ee1e",
                "data": {
                    "name": "Roy's Rolls",
                    "rating": 3,
                    "date": "2015-01-10T00:00:00.0000000Z"
                }
            },
            "right": {
                "hash": "7ff4830",
                "data": {
                    "name": "Prima Doner",
                    "rating": 4,
                    "date": "2015-02-12T00:00:00.0000000Z"
                }
            }
        },
        "right": null
    }
}
```

> **Note**: All leaves have the same depth in the tree:

![](../assets/imgs/03trans.png?raw=true)

![](../assets/imgs/11trans.png?raw=true)

Source: [Stack Exchange](http://bitcoin.stackexchange.com/a/30330)
