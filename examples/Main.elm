module Main exposing (..)

--LIBS

import Html exposing (..)
import Html.Attributes exposing (..)
import Json.Decode exposing (..)
import Json.Encode exposing (..)


--FILES

import Merkle exposing (..)


( a, b, c, d, e ) =
    ( Json.Encode.object
        [ ( "name", Json.Encode.string "Bridge Cafe" )
        , ( "rating", Json.Encode.float 4.0 )
        , ( "date", "2014-02-20T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "Prima Doner" )
        , ( "rating", Json.Encode.float 2.0 )
        , ( "date", "2014-04-15T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "The Bull" )
        , ( "rating", Json.Encode.float 3.0 )
        , ( "date", "2014-06-05T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "The Tall Ship" )
        , ( "rating", Json.Encode.float 5.0 )
        , ( "date", "2014-10-30T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    , Json.Encode.object
        [ ( "name", Json.Encode.string "Roy's Rolls" )
        , ( "rating", Json.Encode.float 3.0 )
        , ( "date", "2015-01-10T00:00:00.0000000Z" |> Json.Encode.string )
        ]
    )
f : Json.Encode.Value
f =
    Json.Encode.object
        [ ( "name", Json.Encode.string "Prima Doner" )
        , ( "rating", Json.Encode.float 4.0 )
        , ( "date", "2015-02-12T00:00:00.0000000Z" |> Json.Encode.string )
        ]


tree : Tree Json.Encode.Value
tree =
    [ a, b, c, d, e ]
        |> Merkle.fromList Nothing identity


tree' : Tree Json.Encode.Value
tree' =
    tree |> Merkle.insert f


json1 : Tree Json.Encode.Value
json1 =
    case
        (tree
            |> Merkle.toJson 0 0
            |> Merkle.fromJson Nothing identity Json.Decode.value
        )
    of
        Ok t ->
            t

        Err msg ->
            Debug.crash msg


json2 : Tree Json.Encode.Value
json2 =
    case
        (tree'
            |> Merkle.toJson 0 0
            |> Merkle.fromJson Nothing identity Json.Decode.value
        )
    of
        Ok t ->
            t

        Err msg ->
            Debug.crash msg


main : Html msg
main =
    div [ style [ ( "font-family", "Courier" ) ] ]
        [ div []
            [ textarea
                [ cols 80
                , rows 20
                ]
                [ text <|
                    "Merkle Tree of Json Values (before f inserted): "
                        ++ (tree |> Merkle.toJson 4 7)
                ]
            , textarea
                [ cols 80
                , rows 20
                ]
                [ text <|
                    "Merkle Tree of Json Values (after f inserted): "
                        ++ (tree' |> Merkle.toJson 4 7)
                ]
            ]
        , div []
            [ textarea
                [ cols 80
                , rows 20
                ]
                [ text <|
                    "Merkle Tree of Json Values (to and from JSON): "
                        ++ (json1 |> Merkle.toJson 4 7)
                ]
            , textarea
                [ cols 80
                , rows 20
                ]
                [ text <|
                    "Merkle Tree of Json Values (to and from JSON): "
                        ++ (json2 |> Merkle.toJson 4 7)
                ]
            ]
        ]
